#!/usr/bin/env python

BASE = '/home/pi/Development/repos/powerMQTT' # A directory to store everything underneath.
LOG_FILE = '%s/output.log' % BASE # Plain text output file.

from Adafruit_ADS1x15 import ADS1x15
import paho.mqtt.client as mqtt
import time
import datetime
import json
import ssl
import logging
import urllib
import urllib2
import RPi.GPIO as GPIO

#Setup for PV pump/water cooling*****************************************************************************************************

GPIO.setmode(GPIO.BCM)

#Set up for the gravity tank float sensor
floatSensorPin=26
GPIO.setup(floatSensorPin, GPIO.IN, pull_up_down=GPIO.PUD_UP) 

#Used to avoid trying to pump at night
minPumpVoltageThreshold=15.0

#Temperatue threshold for swithcing on the water cooling DegC
temperatureCoolingThreshold=35.0

#Set up the water cooling SSR control 
waterCoolSSRControlPin=16
GPIO.setup(waterCoolSSRControlPin,GPIO.OUT)
GPIO.output(waterCoolSSRControlPin,GPIO.LOW)

#Set up the Pump SSR control
pumpSSRControlPin=20
GPIO.setup(pumpSSRControlPin,GPIO.OUT)
GPIO.output(pumpSSRControlPin,GPIO.LOW)

#*********************************************************************************************************************

ambientTempPath = "/sys/bus/w1/devices/28-00043b4451ff/w1_slave"
hwcTempPath = "/sys/bus/w1/devices/28-00043e59ffff/w1_slave"
pvTempPath = "/sys/bus/w1/devices/28-021463275aff/w1_slave"

pvTopic="emon/pv/"
voltsNode="volts"
ampsNode="amps"
wattsNode="watts"
ambientTempNode="ambientTemp"
hwcTempNode="hwcTemp"
pvTempNode="pvTemp"
pumpNode="pump"

adcMultiplier=15.7
oneK=1000

loopDelaySeconds=30

logging.basicConfig(format='%(asctime)s - %(message)s', filename=LOG_FILE, level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S')

def getTemp(source):
	try:
		tfile = open(source) 
		text = tfile.read() 
		tfile.close() 
		secondline = text.split("\n")[1] 
		temperaturedata = secondline.split(" ")[9] 
		temperature = float(temperaturedata[2:]) 
		temperature = temperature / oneK 
		return temperature
	except Exception, e:
		logging.error(e)
		return 0.0

def measureLoop(): 
	try:
		logging.debug("Starting") 

		mqttc = mqtt.Client()
		mqttc.username_pw_set("xxx", "xxx")
		mqttc.connect("x.x.x.x.x", 1883)
		mqttc.loop_start()


		pga = 6144
		sps = 8
		adc = ADS1x15(ic=0x01)

		volts = (adc.readADCSingleEnded(0, pga, sps)/oneK)*adcMultiplier
		mqttc.publish(pvTopic+voltsNode,str(round(volts,1)),2)

		amps = (adc.readADCSingleEnded(1, pga, sps)/oneK)*adcMultiplier
		mqttc.publish(pvTopic+ampsNode,str(round(amps,1)),2)

		watts=volts*amps
		mqttc.publish(pvTopic+wattsNode,str(round(watts,1)),2)

		logging.debug("Got power readings: {0}W, {1}A, {2}V".format(watts,amps,volts))


		ts = time.time()
		dt = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%dT%H:%M:%S')
	

		ambientTemp = getTemp(ambientTempPath)
		mqttc.publish(pvTopic+ambientTempNode,str(round(ambientTemp,1)),2)

		hwcTemp = getTemp(hwcTempPath)
		mqttc.publish(pvTopic+hwcTempNode,str(round(hwcTemp,1)),2)

		pvTemp = getTemp(pvTempPath)
		mqttc.publish(pvTopic+pvTempNode,str(round(pvTemp,1)),2)

		logging.debug("Got temp readings: {0}Deg, {1}Deg, {2}Deg".format(ambientTemp,hwcTemp,pvTemp))


		#Check to see of gravity tank needs filling
		pumpOn = 0
		if (GPIO.input(floatSensorPin) == GPIO.LOW) and (volts > minPumpVoltageThreshold):
			logging.debug("Open - Tank requires filling")
			GPIO.output(pumpSSRControlPin,GPIO.HIGH)
			pumpOn = 1
		else:
			logging.debug("Closed - Tank full or voltage too low")
			GPIO.output(pumpSSRControlPin,GPIO.LOW)

		#Check panels are too hot
		coolingOn = 0
		if (pvTemp > temperatureCoolingThreshold):
			logging.debug("Open - Panels are hot, turning on cooling")
			GPIO.output(waterCoolSSRControlPin,GPIO.HIGH)
			coolingOn = 1
		else:
			logging.debug("Closed - Panels are cool, no need for cooling")
			GPIO.output(waterCoolSSRControlPin,GPIO.LOW)

		mqttc.publish(pvTopic+pumpNode,str(pumpOn),2)
		logging.debug("Got pump reading: {0}".format(pumpOn))


		mqttc.loop_stop()
		mqttc.disconnect()

		logging.debug("Finished, slarting new loop in 30s")
	except Exception, e:
		mqttc.loop_stop()
		mqttc.disconnect()

		logging.error(e)


def startLooping():
	logging.debug('Start looping')
	while True:
		measureLoop()
		try:
			time.sleep(loopDelaySeconds)
		except Exception, e:
			logging.error(e)


startLooping()


